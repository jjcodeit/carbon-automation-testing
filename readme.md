# Automation Testing Project for Carbon Social 
***
## About this Repository
This repository is a codebase that provides a foundation for 
a fully automated browser-based test suite tailored for Carbon social platform. Written in [Python](https://www.python.org/), it uses [Selenium](https://www.selenium.dev/) with 
[Pytest](https://docs.pytest.org/en/6.2.x/). 
This test suite helps to assure that the frontend browser 
functionality (desktop and mobile) is working as expected from the user's standpoint. It makes it easy to locate bugs, 
solve user UI functionality, and check if anything is broken from code updates.

For demo purposes, the test suite currently has 3 finished automation tests under
user `test_login.py` and `test_post.py`. If there is interest from Carbon, the code base is already
structured and ready for further development as needed.

## Some Benefits
* No access to backend code needed for automation (tests are 100% automated through frontend browser, i.e test-developer needs zero access to backend code or APIs)
* Clean code structure:
  * custom code abstraction is used to make test files easily readable and maintainable, even for non-technical people
  * test files are modified by simply changing string parameters for desired results
  * classes are used to create PageObjects (keeps a clean structure for source code)
* test suite can be run with parallel execution using Selenium Grid 4 and [pytest xdist extension](https://pypi.org/project/pytest-xdist/)
* covers testing for web browsers on desktops and mobile.

## Mobile Automation Test Suite
Note that everything above can be done for mobile apps as well, including iOS and Android
using [Appium](https://appium.io/), which uses Selenium API under the hood. However, it requires much more work.


# A Little About me
My name is JJ Brooks. I'm software developer from Canada, currently living in Taiwan. Previously, between 2015-2018, I was hired as a blockchain consultant
in Singapore for DDKoin in the early stages of its development. I later went on in 2018 to launch [OOLi Network, a Singapore Startup](https://opengovsg.com/corporate/201841930K)
with an initial funding of USD 250,000. For this startup, I designed a complete concept for a social network app (iOS/Android) and managed a team of developers to complete and launch the project.
Our stack used Django API framework, OrientDB for NOSQL and graph features, and React Native for iOS and Android. Aside from project management, my development role was to focus on automation testing.  
Unfortunately, due to the financial panic of Covid-19 the project was no longer sustainable and the project and company were closed in 2020. 

# My Interest In Carbon
I was introduced to your platform from a friend because of my known interest in crypto/blockchain
and because I myself founded a social media network, thus your project sparked an interest. 
From my experience, testing is something startups struggle with due to the initial overwhelming need to develop new features, bugs, and MOST importantly... marketing.
I could help contribute test suite development for frontend UI automation. All wouldn't mind sharing some of my experience.

# My Proposal 
It is actually quite simple. I'm exploring the idea to help you with testing your user interface using automation. And because I can imagine budgeting is an issue, I'd accept receiving compensation through your CRBN coin. If you are interested we can discuss the details from there. I like your project and think we might share some worthy interests.

You can contact me at jj_brooks@icloud.com if you are interested and
perhaps have a Zoom call or something. I also wouldn't mind sharing some of my experiences, obstacles, and lessons learned for my social app and crypto startup experiences. 

Regards and all the best,
JJ Brooks

