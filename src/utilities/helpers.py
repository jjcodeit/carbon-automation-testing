from src.page_models.auth import LoginPage
from src.utilities.waiters import Waiter
from src.utilities.urls import Url
from typing import Callable


class Helper:
    """
    This is a utility class for miscellaneous methods not specific to a single class.
    """
    @staticmethod
    def get_url_helper(driver: Callable, url: str):
        driver = driver
        return driver.get(url)

    @staticmethod
    def login_helper(driver: Callable, username: str, password: str):
        """
        Use this function if your test needs login automation before testing like in the
        case when testing post functionality which requires a logged in user.
        If you don't need pre login automation, just comment this function out from
        test code.
        """
        driver = driver
        Helper.get_url_helper(driver, Url.login_page)

        Waiter().element_clickable(driver, 30, 'id', 'root')
        LoginPage(driver).input_username_and_password(f"{username}", f"{password}")
        LoginPage(driver).click_signin_button('crbnauthsigninbutton')
        # need time waiter here

    @staticmethod
    def delete_post_helper(driver: Callable):

        driver = driver
        pass


    @staticmethod
    def create_post_helper(driver: Callable):
        driver = driver
        pass