from typing import Callable


class LoginPage:

    def __init__(self, driver: Callable):
        self.driver = driver

    def click_login_button(self, login_button: str):
        login_button_to_next_page = self.driver.find_element_by_class_name(login_button).click()

    def input_username_and_password(self, username: str, password: str):
        page_inputs = self.driver.find_elements_by_tag_name('input')
        username_input, password_input = page_inputs[1], page_inputs[2]
        username_input.send_keys(username)
        password_input.send_keys(password)

    def click_signin_button(self, element: str):
        self.driver.find_element_by_class_name(element).click()

    def check_is_username_in_header(self, element: str): 
        username_header = self.driver.find_element_by_class_name(element)
        return username_header

    def get_login_error_message(self, element):
        error_message_element = self.driver.find_element_by_class_name(element)
        if error_message_element:
            return error_message_element
