import pytest 
from src.page_models.auth import LoginPage
from src.utilities.drivers import Driver
from src.utilities.waiters import Waiter
from src.utilities.urls import Url


class TestLogin:
    """ All tests here are for login-authentication of 'existing users' only. """

    # do not move fixture to conftest.py!
    @pytest.fixture(scope="function")
    def driver(self):
        # Set desired driver here from drivers.py before running test
        driver = Driver.local_firefox_web_driver(headless=True)
        yield driver
        driver.quit()

    # set login credentials here:
    username = 'add username'
    password = 'add password'

    @staticmethod
    def test_successful_user_login(driver):
        """
        Testing if user login is successful.
        """
        driver.get(Url.index_page)

        Waiter().element_clickable(driver, 30, 'css selector', 'div.carbonwelcomebannerlogin')
        LoginPage(driver).click_login_button("carbonwelcomebannerlogin")
        
        Waiter().element_clickable(driver, 30, 'id', 'root')
        LoginPage(driver).input_username_and_password(TestLogin.username, TestLogin.password)
        LoginPage(driver).click_signin_button('crbnauthsigninbutton')
        
        Waiter().element_clickable(driver, 30, 'css selector', 'a.headerusername')
        username_in_header = LoginPage(driver).check_is_username_in_header('headerusername')
        # driver.quit()

        # THEN username should be located in the header after user is successfully logged in
        assert username_in_header.text == f"@{TestLogin.username}"

    @staticmethod
    def test_invalid_user_login(driver):
        """
        Test should return PASSED if login is invalid.
        This confirms that the expected result is achieved.
        """
        driver.get(Url.login_page)
        driver.delete_all_cookies()

        Waiter().element_clickable(driver, 30, 'id', 'root')
        LoginPage(driver).input_username_and_password("add username", "add password")
        LoginPage(driver).click_login_button('crbnauthsigninbutton')
        # driver.quit()
        
        # when login is successful the user is redirected to the index page
        # if this test PASSED, it means the user is still on current login page because authentication failed
        assert not Url.index_page == driver.current_url
