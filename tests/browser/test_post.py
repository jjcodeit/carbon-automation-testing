import pytest
from src.page_models.post import CreatePost
from src.utilities.waiters import Waiter
from src.utilities.helpers import Helper
from src.utilities.drivers import Driver


class TestPost:
    """ All tests here are for post related issues only."""
    post_text = "UI looks good!"

    @pytest.fixture(scope="function")
    def driver(self):
        # Set desired driver here from Driver class in drivers.py before running test
        driver = Driver.local_firefox_web_driver(headless=True)
        return driver

    @staticmethod
    def test_text_post_only(driver):
        """Testing if text-only post is created successful."""

        #pre-login helper before running post test
        Helper.login_helper(driver, username="add username", password="add password")

        # open post editor
        Waiter.element_is_visible(driver, 30, "css selector", "div.carboncontentshare")
        CreatePost(driver).click_post_editor("div.carboncontentshare")

        # create post
        Waiter.element_is_visible(driver, 30, "id", "createcontenttextinput")
        CreatePost(driver).enter_post_text(TestPost.post_text, "createcontenttextinput")
        CreatePost(driver).click_post_button("postbutton")

        # check if new post exists with text
        Waiter.element_is_visible(driver, 30, "css selector", "div.socialpostcontentlive")
        new_post_text = CreatePost(driver).get_text_from_new_post("div.socialpostcontentlive")

        # THEN post text should exist if post is successful
        assert new_post_text == TestPost.post_text

    @staticmethod
    def test_post_with_image(driver):
        pass

    @staticmethod
    def test_post_with_gif(driver):
        pass

    @staticmethod
    def test_delete_post(self):
        pass
    