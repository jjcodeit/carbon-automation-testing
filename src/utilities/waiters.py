from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions 
from typing import Callable


class Waiter:

    @staticmethod
    def element_clickable(driver: Callable, timeout: int, css_selector: str, element: str):
        waiter = WebDriverWait(driver, timeout)
        waiter.until(expected_conditions.element_to_be_clickable((f'{css_selector}', f'{element}')))

    @staticmethod
    def element_is_visible(driver: Callable, timeout: int, css_selector: str, element: str):
        waiter = WebDriverWait(driver, timeout)
        waiter.until(expected_conditions.visibility_of_element_located((f'{css_selector}', f'{element}')))
