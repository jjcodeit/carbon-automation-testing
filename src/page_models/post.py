from typing import Callable


class CreatePost:
    """
    This class provides methods related to creating posts. Using these methods make test code
    easier to read and understand. Any new code related to creating posts should be added here.
    """

    def __init__(self, driver: Callable):
        """
        The driver is passed to the instance in the test code.
        This will solve the find_element_by_* methods below that can not find the driver before being called reference here.
        Nothing to worry about.
        """
        self.driver = driver

    def click_post_editor(self, editor_css_selector: str):
        content_share = self.driver.find_element_by_css_selector(editor_css_selector)
        content_share.click()

    def enter_post_text(self, text: str, post_text_id):
        """This function first clears any existing text from input and then type in
        desired text passed by the argument."""
        text_input = self.driver.find_element_by_id(post_text_id)
        text_input.clear()
        text_input.send_keys(text)

    def click_post_button(self, post_button_class_name: str):
        self.driver.find_element_by_class_name(post_button_class_name).click()

    def get_text_from_new_post(self, posted_text_css_selector: str):
        """This function will return the text taken from the post."""
        posted_text = self.driver.find_element_by_css_selector(posted_text_css_selector)
        return posted_text.text








