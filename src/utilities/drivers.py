from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import os
from dotenv import load_dotenv

load_dotenv()


class Driver:

    # for local drivers using os.environ make sure to set ROOT path in .env based on local machine's
    # absolute path to project. Failing to do so will lead to a error: selenium.common.exceptions.WebDriverException

    @staticmethod
    def get_local_chrome_web_driver():
        return webdriver.Chrome(executable_path=str(os.environ.get('CHROME_LOCAL_DRIVER')),
                                service_log_path=str(os.environ.get('CHROME_LOG_FILES'))
                                )

    @staticmethod
    def get_local_firefox_web_driver():
        return webdriver.Firefox(executable_path=str(os.environ.get('FIREFOX_LOCAL_DRIVER')),
                                 service_log_path=str(os.environ.get('FIREFOX_LOG_FILES'))
                                 )

    @staticmethod
    def set_remote_webdriver_chrome(grid_url: str):
        return webdriver.Remote(command_executor=grid_url, desired_capabilities=DesiredCapabilities.CHROME)

    @staticmethod
    def set_remote_webdriver_firefox(grid_url: str):
        return webdriver.Remote(command_executor=grid_url, desired_capabilities=DesiredCapabilities.FIREFOX)

    # @staticmethod
    # def set_remote_webdriver_ie(grid_url: str):
    #     return webdriver.IeOptions()
