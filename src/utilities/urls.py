

class Url:
    """
    All urls are added and referenced from this class.
    """
    hub = "http://192.168.56.100:4444/wd/hub"
    index_page = "https://crbn.social"
    login_page = "https://crbn.social/signin"
